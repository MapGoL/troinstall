package main

import (
	"io/ioutil"
	"os"
)

func Create(fileName string, nr string) {
	content := []byte(nr)
	err := ioutil.WriteFile(fileName, content, 0644)
	if err != nil {
		panic(err)
	}
}
func Reader(fileName string) []byte {

	file, err := os.Open(fileName)
	if err != nil {
		panic(err)
	}
	defer file.Close()
	content, err := ioutil.ReadAll(file)
	//log.Print(string(content))
	return content
}
func Exists(path string) bool {
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return true
}
