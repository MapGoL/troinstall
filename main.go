package main

import (
	"bufio"
	"encoding/json"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	config := NewTroConfig()
	print("输入域名：")
	domain := EnterCMD()
	config.LocalAddr = domain[1][0]
	config.RemoteAddr = domain[1][0]
	config.Ssl.Sni = domain[1][0]
	print("远程端口:")
	rp := EnterCMD()
	print("本地端口：")
	lp := EnterCMD()
	config.RemotePort, _ = strconv.Atoi(rp[1][0])
	config.LocalPort, _ = strconv.Atoi(lp[1][0])

	log.Println("是否使用Ws(Y/n)")
	enter := EnterCMD()
	if enter[1][0] == "y" || enter[1][0] == "Y" {
		config.Websocket.Enabled = true
		config.Websocket.Host = domain[1][0]
		log.Println("是否使用ss加密(Y/n)")
		ifss := EnterCMD()
		if ifss[1][0] == "y" || ifss[1][0] == "Y" {
			config.Shadowsocks.Enabled = true
		}
	}
	log.Println("Config:\n", InterfaceToJson(config))
	Create("config.json", InterfaceToJson(config))
	if !Exists("/etc/nginx/nginx.conf") {
		println("ERROR:不存在Nginx!")
		return
	}
	println("存在Nginx (√)")
	if enter[1][0] == "y" || enter[1][0] == "Y" {
		println("启用Ws （√）")
		println("使用Nginx配置WSS")
		bconf := Reader("nginx.conf")
		sconf := string(bconf)
		println("config:\n",sconf)
		newconf := strings.ReplaceAll(strings.ReplaceAll(sconf,"$server_name",domain[1][0]),"$local_port",lp[0][1])
		Create("/etc/nginx/conf/tro.conf",newconf)
		log.Println("/etc/nginx/conf/tro.conf")
		


	}

}

func InterfaceToJson(param interface{}) string {
	dataType, _ := json.Marshal(param)
	dataString := string(dataType)
	return dataString
}

func EnterCMD() [][]string {
	ans := make([][]string, 1)
	sca := bufio.NewScanner(os.Stdin)
	for i := 0; i < 1; i++ {
		if sca.Scan() {
			strSlice := strings.Split(sca.Text(), " ")
			nums := make([]string, 0)
			for _, v := range strSlice {
				num := v
				nums = append(nums, num)
			}
			ans = append(ans, nums)
		}
	}

	return ans
}
