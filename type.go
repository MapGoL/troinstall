package main

import "encoding/json"

type TroConfig struct {
	RunType    string   `json:"run_type"`
	LocalAddr  string   `json:"local_addr"`
	LocalPort  int      `json:"local_port"`
	RemoteAddr string   `json:"remote_addr"`
	RemotePort int      `json:"remote_port"`
	Password   []string `json:"password"`
	Ssl        struct {
		Cert string `json:"cert"`
		Key  string `json:"key"`
		Sni  string `json:"sni"`
	} `json:"ssl"`
	Websocket struct {
		Enabled bool   `json:"enabled"`
		Path    string `json:"path"`
		Host    string `json:"host"`
	} `json:"websocket"`
	Shadowsocks struct {
		Enabled  bool   `json:"enabled"`
		Password string `json:"password"`
	} `json:"shadowsocks"`
	Router struct {
		Enabled bool     `json:"enabled"`
		Block   []string `json:"block"`
		Geoip   string   `json:"geoip"`
		Geosite string   `json:"geosite"`
	} `json:"router"`
}

func NewTroConfig() *TroConfig {
	c := Reader("config")
	tro := &TroConfig{}
	json.Unmarshal(c,tro)
	tro.Websocket.Enabled = false
	tro.Shadowsocks.Enabled = false
	return tro
}